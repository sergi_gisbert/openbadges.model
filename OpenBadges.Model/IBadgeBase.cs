﻿using System;

namespace OpenBadges.Model
{
    /// <summary>
    /// Base Badge definition
    /// </summary>
    /// 10/3/2013 by Sergi
    public interface IBadgeBase
    {
        /// <summary>
        /// Serializes the object to a JSON string.
        /// </summary>
        /// <param name="indented">if true, the output string is indented.</param>
        /// <returns></returns>
        /// 10/3/2013 by Sergi
        string ToJson(Boolean indented = false);
    }
}