﻿using System;
using Newtonsoft.Json;

namespace OpenBadges.Model.Issue
{
    /// <summary>
    /// Alignment Object definition
    /// </summary>
    /// 10/3/2013 by Sergi
    public class AlignmentObject
    {
        #region " Properties "

        /// <summary>
        /// Name of the alignment.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("name")]
        public String Name { get; set; }

        /// <summary>
        /// URL linking to the official description of the standard.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("url")]
        public String Url { get; set; }

        /// <summary>
        /// Short description of the standard
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("description")]
        public String Description { get; set; }

        #endregion

    }
}