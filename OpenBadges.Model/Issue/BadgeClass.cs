﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace OpenBadges.Model.Issue
{
    /// <summary>
    /// A badge representation
    /// </summary>
    /// 10/3/2013 by Sergi
    public sealed class BadgeClass : BadgeBase<BadgeClass>, IBadgeClass
    {
        #region " Properties "

        /// <summary>
        /// The name of the achievement.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("name")]
        public String Name { get; set; }

        /// <summary>
        /// A short description of the achievement.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("description")]
        public String Description { get; set; }

        /// <summary>
        /// URL of an image representing the achievement.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("image")]
        public String Image { get; set; }

        /// <summary>
        /// URL of the criteria for earning the achievement. If the badge represents an educational achievement, consider marking up this up with LRMI
        /// </summary>
        /// <value>
        /// The criteria.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("criteria")]
        public String Criteria { get; set; }

        /// <summary>
        /// URL of the organization that issued the badge. Endpoint should be an IssuerOrganization
        /// </summary>
        /// <value>
        /// The issuer.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("issuer")]
        public String Issuer { get; set; }

        /// <summary>
        /// List of objects describing which educational standards this badge aligns to, if any.
        /// </summary>
        /// <value>
        /// The alignment.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("alignment")]
        public List<AlignmentObject> Alignment { get; set; }

        /// <summary>
        /// List of tags that describe the type of achievement.
        /// </summary>
        /// <value>
        /// The tags.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("tags")]
        public List<String> Tags { get; set; }

        #endregion

        #region " Constructors "

        /// <summary>
        /// Initializes a new instance of class <see cref="BadgeClass"/>.
        /// </summary>
        /// 10/3/2013 by Sergi
        public BadgeClass()
        {
        }

        /// <summary>
        /// Initializes a new instance of class <see cref="BadgeClass"/>.
        /// </summary>
        /// <param name="jsonString">The json string to deserialize the object from.</param>
        /// 10/3/2013 by Sergi
        public BadgeClass(String jsonString)
        {
            var temp = Load(jsonString);
            if (temp == null) return;

            Name = temp.Name;
            Description = temp.Description;
            Image = temp.Image;
            Criteria = temp.Criteria;
            Issuer = temp.Issuer;
            Alignment = temp.Alignment;
            Tags = temp.Tags;
        }

        #endregion
    }
}