﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace OpenBadges.Model.Issue
{
    /// <summary>
    /// A badge representation
    /// </summary>
    /// 10/3/2013 by Sergi
    public interface IBadgeClass : IBadgeBase
    {
        /// <summary>
        /// The name of the achievement.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("name")]
        String Name { get; set; }

        /// <summary>
        /// A short description of the achievement.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("description")]
        String Description { get; set; }

        /// <summary>
        /// URL of an image representing the achievement.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("image")]
        String Image { get; set; }

        /// <summary>
        /// URL of the criteria for earning the achievement. If the badge represents an educational achievement, consider marking up this up with LRMI
        /// </summary>
        /// <value>
        /// The criteria.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("criteria")]
        String Criteria { get; set; }

        /// <summary>
        /// URL of the organization that issued the badge. Endpoint should be an IssuerOrganization
        /// </summary>
        /// <value>
        /// The issuer.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("issuer")]
        String Issuer { get; set; }

        /// <summary>
        /// List of objects describing which educational standards this badge aligns to, if any.
        /// </summary>
        /// <value>
        /// The alignment.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("alignment")]
        List<AlignmentObject> Alignment { get; set; }

        /// <summary>
        /// List of tags that describe the type of achievement.
        /// </summary>
        /// <value>
        /// The tags.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("tags")]
        List<String> Tags { get; set; }
    }
}