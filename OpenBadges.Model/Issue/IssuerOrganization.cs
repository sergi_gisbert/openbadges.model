﻿using System;
using Newtonsoft.Json;

namespace OpenBadges.Model.Issue
{
    /// <summary>
    /// Organization that issues a Badge
    /// </summary>
    /// 10/3/2013 by Sergi
    public class IssuerOrganization : BadgeBase<IssuerOrganization>, IIssuerOrganization
    {
        #region " Properties "

        /// <summary>
        /// The name of the issuing organization.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("name")]
        public String Name { get; set; }

        /// <summary>
        /// URL of the institution
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("url")]
        public String Url { get; set; }

        /// <summary>
        /// A short description of the institution
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("description")]
        public String Description { get; set; }

        /// <summary>
        /// An image representing the institution
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("image")]
        public String Image { get; set; }

        /// <summary>
        /// Contact address for someone at the organization.
        /// </summary>
        /// <value>
        /// The email.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("email")]
        public String Email { get; set; }

        /// <summary>
        /// URL of the Badge Revocation List. The endpoint should be a JSON representation of an object where the keys are the uid a revoked badge assertion, 
        /// and the values are the reason for revocation. This is only necessary for signed badges.
        /// </summary>
        /// <value>
        /// The revocation list.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("revocationList")]
        public String RevocationList { get; set; }

        #endregion

        #region " Constructors "

        /// <summary>
        /// Initializes a new instance of class <see cref="IssuerOrganization"/>.
        /// </summary>
        /// 10/3/2013 by Sergi
        public IssuerOrganization()
        {
        }

        /// <summary>
        /// Initializes a new instance of class <see cref="IssuerOrganization"/>.
        /// </summary>
        /// <param name="jsonString">The json string to deserialize the object from.</param>
        /// 10/3/2013 by Sergi
        public IssuerOrganization(String jsonString)
        {
            var temp = Load(jsonString);
            if (temp == null) return;

            Name = temp.Name;
            Url = temp.Url;
            Description = temp.Description;
            Image = temp.Image;
            Email = temp.Email;
            RevocationList = temp.RevocationList;
        }

        #endregion

    }
}