﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace OpenBadges.Model.Assertion
{
    /// <summary>
    /// Identity Object definition
    /// </summary>
    /// 10/3/2013 by Sergi
    public class IdentityObject
    {
        #region " Attributes "

        private IdentityType _type = IdentityType.email;

        #endregion

        #region " Properties "

        /// <summary>
        /// Either the hash of the identity or the plaintext value. If it's possible that the plaintext transmission and storage of the 
        /// identity value would leak personally identifiable information, it is strongly recommended that an IdentityHash be used.
        /// </summary>
        /// <value>
        /// The identity.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("identity")]
        public String Identity { get; set; }

        /// <summary>
        /// The type of identity.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public IdentityType Type
        {
            get { return _type; }
            set { _type = value; }
        }

        /// <summary>
        /// Whether or not the id value is hashed.
        /// </summary>
        /// <value>
        /// The hashed.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("hashed")]
        public Boolean Hashed { get; set; }

        /// <summary>
        /// If the recipient is hashed, this should contain the string used to salt the hash. 
        /// If this value is not provided, it should be assumed that the hash was not salted.
        /// </summary>
        /// <value>
        /// The salt.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("salt")]
        public String Salt { get; set; }

        #endregion

    }
}