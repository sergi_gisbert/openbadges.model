﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace OpenBadges.Model.Assertion
{
    /// <summary>
    /// Verification Object definition
    /// </summary>
    /// 10/3/2013 by Sergi
    public class VerificationObject
    {
        #region " Properties "

        /// <summary>
        /// The type of verification method.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("type")]
        [JsonConverter(typeof(StringEnumConverter))]
        public VerificationType Type { get; set; }

        /// <summary>
        /// If the type is "hosted", this should be a URL pointing to the assertion on the issuer's server. 
        /// If the type is "signed", this should be a link to the issuer's public key.
        /// </summary>
        /// <value>
        /// The URL.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("url")]
        public String Url { get; set; }

        #endregion

    }
}