﻿using System;
using Newtonsoft.Json;

namespace OpenBadges.Model.Assertion
{
    /// <summary>
    /// Assertions are representations of an awarded badge
    /// </summary>
    /// 10/3/2013 by Sergi
    public sealed class BadgeAssertion : BadgeBase<BadgeAssertion>, IBadgeAssertion
    {
        #region " Properties "

        /// <summary>
        /// Unique Identifier for the badge. This is expected to be locally unique on a per-origin basis, not globally unique.
        /// </summary>
        /// <value>
        /// The unique identifier.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("uid")]
        public String Id { get; set; }

        /// <summary>
        /// The recipient of the achievement.
        /// </summary>
        /// <value>
        /// The recipient.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("recipient")]
        public IdentityObject Recipient { get; set; }

        /// <summary>
        /// URL that describes the type of badge being awarded. The endpoint should be a BadgeClass
        /// </summary>
        /// <value>
        /// The badge.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("badge")]
        public String Badge { get; set; }

        /// <summary>
        /// Data to help a third party verify this assertion.
        /// </summary>
        /// <value>
        /// The verify.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("verify")]
        public VerificationObject Verify { get; set; }

        /// <summary>
        /// Date that the achievement was awarded.
        /// </summary>
        /// <value>
        /// The issued configuration.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("issuedOn")]
        public String IssuedOn { get; set; }

        /// <summary>
        /// URL of an image representing this user's achievement. This should be a PNG image, and if possible, the image should be prepared via the Baking specification.
        /// </summary>
        /// <value>
        /// The image.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("image")]
        public String Image { get; set; }

        /// <summary>
        /// URL of the work that the recipient did to earn the achievement. This can be a page that links out to other pages if linking directly to the work is infeasible.
        /// </summary>
        /// <value>
        /// The evidence.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("evidence")]
        public String Evidence { get; set; }

        /// <summary>
        /// If the achievment has some notion of expiry, this indicates when a badge should no longer be considered valid.
        /// </summary>
        /// <value>
        /// The expires.
        /// </value>
        /// 10/3/2013 by Sergi
        [JsonProperty("expires")]
        public DateTime? Expires { get; set; }

        #endregion

        #region " Constructors "

        /// <summary>
        /// Initializes a new instance of class <see cref="BadgeAssertion"/>.
        /// </summary>
        /// 10/3/2013 by Sergi
        public BadgeAssertion()
        {
        }

        /// <summary>
        /// Initializes a new instance of class <see cref="BadgeAssertion"/>.
        /// </summary>
        /// <param name="jsonString">The json string to deserialize the object from.</param>
        /// 10/3/2013 by Sergi
        public BadgeAssertion(String jsonString)
        {
            BadgeAssertion temp = Load(jsonString);
            if (temp == null) return;

            Id = temp.Id;
            Recipient = temp.Recipient;
            Badge = temp.Badge;
            Verify = temp.Verify;
            IssuedOn = temp.IssuedOn;
            Image = temp.Image;
            Evidence = temp.Evidence;
            Expires = temp.Expires;
        }

        #endregion
    }
}