﻿namespace OpenBadges.Model.Assertion
{
    public enum VerificationType
    {
        hosted,
        signed
    }
}