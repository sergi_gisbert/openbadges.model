﻿using System;
using Newtonsoft.Json;

namespace OpenBadges.Model
{
    /// <summary>
    /// Base Badge class
    /// </summary>
    /// <typeparam name="T">Base type to deserialize</typeparam>
    /// 10/3/2013 by Sergi
    public abstract class BadgeBase<T> : IBadgeBase
    {
        #region " Private attributes "

        /// <summary>
        /// The default serialization ettings
        /// </summary>
        /// 10/3/2013 by Sergi
        protected static readonly JsonSerializerSettings _settings = new JsonSerializerSettings
                                                                         {
                                                                             DateFormatHandling =
                                                                                 DateFormatHandling.IsoDateFormat,
                                                                             DateParseHandling =
                                                                                 DateParseHandling.DateTime,
                                                                             MissingMemberHandling =
                                                                                 MissingMemberHandling.Ignore,
                                                                             NullValueHandling =
                                                                                 NullValueHandling.Ignore,
                                                                         };

        #endregion

        #region " Common Methods "

        /// <summary>
        /// Serializes the object to a JSON string.
        /// </summary>
        /// <param name="indented">if true, the output string is indented.</param>
        /// <returns></returns>
        /// 10/3/2013 by Sergi
        public virtual string ToJson(Boolean indented = false)
        {
            return JsonConvert.SerializeObject(this, indented ? Formatting.Indented : Formatting.None, _settings);
        }

        /// <summary>
        /// Serializes the object to a JSON string.
        /// </summary>
        /// <returns></returns>
        /// 10/3/2013 by Sergi
        public override String ToString()
        {
            return ToJson();
        }

        /// <summary>
        /// Loads an object from a JSON string.
        /// </summary>
        /// <param name="json">The json string.</param>
        /// <returns></returns>
        /// 10/3/2013 by Sergi
        public static T Load(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, _settings);
        }

        #endregion
    }
}