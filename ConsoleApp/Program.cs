﻿using System;
using System.Collections.Generic;
using OpenBadges.Model.Assertion;
using OpenBadges.Model.Issue;

namespace ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Sample code for Assertions
            AssertionSample();

            // Sample code for BadgeClass
            BadgeClassSample();

            // Sample code for Issuer Organizations
            IssuerSample();
        }

        /// <summary>
        /// Sample code for Assertions
        /// </summary>
        /// 10/3/2013 by Sergi
        private static void AssertionSample()
        {
            // Badge Assertion sample JSON string
            const string json = @"{
                            ""uid"": ""f2c20"",
                            ""recipient"": 
                                {   ""type"": ""email"",
                                    ""hashed"": true,
                                    ""salt"": ""deadsea"",
                                    ""identity"": ""sha256$c7ef86405ba71b85acd8e2e95166c4b111448089f2e1599f42fe1bba46e865c5""
                                },
                            ""image"": ""https://example.org/beths-robot-badge.png"",
                            ""evidence"": ""https://example.org/beths-robot-work.html"",
                            ""issuedOn"": 1359217910,
                            ""badge"": ""https://example.org/robotics-badge.json"",
                            ""verify"": 
                                {   ""type"": 
                                    ""hosted"",
                                    ""url"": ""https://example.org/beths-robotics-badge.json""
                                }
                            }";

            // Build a BadgeAssertion from a JSON string
            // via constructor:
            IBadgeAssertion badgeAssertion = new BadgeAssertion(json);
            // via static load method:
            IBadgeAssertion badge = BadgeAssertion.Load(json);

            // Convert the BadgeAssertion object to JSON (plain & indented)
            Console.WriteLine(badgeAssertion.ToJson());
            Console.WriteLine(badgeAssertion.ToJson(indented: true));

            // ToString method is overrided to get the JSON string representing the badge, as well
            Console.WriteLine(badgeAssertion);
        }

        /// <summary>
        /// Sample code for BadgeClass
        /// </summary>
        /// 10/3/2013 by Sergi
        private static void BadgeClassSample()
        {
            // Badge Class sample JSON string
            const string badgeClassJson = @"{
                                      ""name"": ""Awesome Robotics Badge"",
                                      ""description"": ""For doing awesome things with robots that people think is pretty great."",
                                      ""image"": ""https://example.org/robotics-badge.png"",
                                      ""criteria"": ""https://example.org/robotics-badge.html"",
                                      ""tags"": [""robots"", ""awesome""],
                                      ""issuer"": ""https://example.org/organization.json"",
                                      ""alignment"": [
                                        { ""name"": ""CCSS.ELA-Literacy.RST.11-12.3"",
                                          ""url"": ""http://www.corestandards.org/ELA-Literacy/RST/11-12/3"",
                                          ""description"": ""Follow precisely a complex multistep procedure when carrying out experiments, taking measurements, or performing technical tasks; analyze the specific results based on explanations in the text.""
                                        },
                                        { ""name"": ""CCSS.ELA-Literacy.RST.11-12.9"",
                                          ""url"": ""http://www.corestandards.org/ELA-Literacy/RST/11-12/9"",
                                          ""description"": "" Synthesize information from a range of sources (e.g., texts, experiments, simulations) into a coherent understanding of a process, phenomenon, or concept, resolving conflicting information when possible.""
                                        }
                                      ]
                                    }";

            // Build a BadgeClass from a JSON string
            // via constructor:
            IBadgeClass badgeClass = new BadgeClass(badgeClassJson);
            // via static load method:
            var badgeClass2 = BadgeClass.Load(badgeClassJson);

            // Convert the BadgeAssertion object to JSON (plain & indented)
            Console.WriteLine(badgeClass.ToJson());
            Console.WriteLine(badgeClass.ToJson(true));

            // ToString method is overrided to get the JSON string representing the badge, as well
            Console.WriteLine(badgeClass);
        }

        /// <summary>
        /// Sample code for Issuer Organizations
        /// </summary>
        /// 10/3/2013 by Sergi
        private static void IssuerSample()
        {
            // Badge Issuer sample JSON string
            const string issuerJson = @"{ 
                                  ""name"": ""An Example Badge Issuer"",
                                  ""image"": ""https://example.org/logo.png"",
                                  ""url"": ""https://example.org"",
                                  ""email"": ""steved@example.org"",
                                  ""revocationList"": ""https://example.org/revoked.json""
                                }";

            // Build a IssuerOrganization from a JSON string
            // via constructor:
            IIssuerOrganization issuer = new IssuerOrganization(issuerJson);
            // via static load method:
            var issuer2 = IssuerOrganization.Load(issuerJson);

            // Convert the BadgeAssertion object to JSON (plain & indented)
            Console.WriteLine(issuer.ToJson());
            Console.WriteLine(issuer.ToJson(true));

            // ToString method is overrided to get the JSON string representing the badge, as well
            Console.WriteLine(issuer);
        }
    }
}